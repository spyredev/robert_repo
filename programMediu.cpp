#include <iostream>

using namespace std;

int main(){
 
 
    cout <<"Nr elemente multime 1: ";
    
    int elemente1, i;
    cin >> elemente1;
    
    int multime1[elemente1];
    
    for(i=1; i<=elemente1; i++){
        cout <<"Elemente = "; cin >> multime1[i];
    }
    
    int elemente2;
    cout <<"Elemente multime 2: ";
    cin >> elemente2;
    
    int multime2[elemente2];
    for(i=1; i<=elemente2;  i++){
        cout <<"Elemente = "; cin >> multime2[i];
    }
    
    
    cout <<"Multimile sunt: ";
    for(i=1; i<=elemente1; i++){
        cout <<multime1[i]<<", ";
    }
    cout << endl;
    for(i=1; i<=elemente2; i++){
        cout <<multime2[i]<<", ";
    }
    cout << endl;
    
    cout << endl;
    cout <<"M1 INTERSECTIE M2 = ";
    int j;
    
    for(i=1; i<=elemente1; i++){
        int elementIdentic = 0;
        for(j=1; j<=elemente2; j++){
            if(multime1[i] == multime2[j]){
                elementIdentic = 1;
            }
        }
        if(elementIdentic == 1){
            cout <<"ELEMENT DIN AMBELE MULTIMI: " << multime1[i] << endl;
        }
    }
       
    return 0;
}